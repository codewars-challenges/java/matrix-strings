package be.hics.sandbox.matrixstrings;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Matrix {

    private final String string;
    private final List<List<Integer>> matrix = new ArrayList<>();

    public Matrix(String matrixAsString) {
        string = matrixAsString;
        if (Objects.nonNull(string)) {
            matrix.addAll(Stream.of(string.split("\r\n|\r|\n")).map(row -> Stream.of(row.split("\\s")).map(s -> Integer.valueOf(s.trim())).collect(Collectors.toList())).collect(Collectors.toList()));
        }
    }

    public int[] getRow(int rowNumber) {
        return matrix.get(rowNumber).stream().mapToInt(Integer::intValue).toArray();
    }

    public int[] getColumn(int columnNumber) {
        return matrix.stream().map(row -> row.get(columnNumber)).mapToInt(Integer::intValue).toArray();
    }

    public int getRowsCount() {
        return matrix.size();
    }

    public int getColumnsCount() {
        return matrix.size() > 0 ? matrix.get(0).size() : 0;
    }

}
