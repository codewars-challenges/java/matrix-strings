package be.hics.sandbox.matrixstrings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatrixStringsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatrixStringsApplication.class, args);
	}
}
